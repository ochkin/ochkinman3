from django.db import models
from django.contrib.auth.models import User
from datetime import datetime
from threading import Timer

CAPACITIES = (
    (2, '2'),
    (4, '4')
)

STATES = (
    (0, 'Inception'),
    (1, 'GameOn'),
    (2, 'GameOver')
)

class Lobby(models.Model):
    Name = models.CharField(max_length=70, unique=True)
    Capacity = models.IntegerField(choices=CAPACITIES)
    Created = models.DateTimeField(auto_now_add=True)
    GameStarted = models.DateTimeField(null=True, blank=True)
    Gamers = models.ManyToManyField(User, related_name='lobbies')
    State = models.IntegerField(choices=STATES)
    Creator = models.ForeignKey(User, on_delete=models.CASCADE, related_name='createdLobbies')

    def __str__(self):         
        return "%s (%s) - %s" % (self.Name, ', '.join([str(usr) for usr in self.Gamers.all()]), STATES[self.State][1])
    def AddGamer(self, usr):
        if (not self.HasGamer(usr)) and (self.State == 0) and (self.Gamers.count() < self.Capacity) and (Violation.IsGamerOk(usr)):
            self.Gamers.add(usr)
    def RemoveGamer(self, usr):
        if (self.HasGamer(usr)) and (self.State != 2) and (Violation.IsGamerOk(usr)):
            if self.State == 1:
                Violation.objects.create(Gamer=usr, QuitGameAt=datetime.today())
            self.Gamers.remove(usr)          
    def HasGamer(self, usr):
        return self.Gamers.filter(id=usr.id).exists()
    def Start(self):
        if self.Capacity == self.Gamers.count():
            self.State = 1
            self.GameStated = datetime.today()
            self.save()
            # set timer to finish game in 1 min
            timer = Timer(60.0, self.Stop)
            timer.start()
            return True
        else:
            return False
    def Stop(self):
        self.State = 2
        self.save()

from django.db.models import Max
#from django.utils import timezone

class Violation(models.Model):
    Gamer = models.ForeignKey(User, on_delete=models.CASCADE, related_name='violations')
    QuitGameAt = models.DateTimeField(null=False, blank=False)
    def IsGamerOk(usr):
        lastViolation = usr.violations.aggregate(Max('QuitGameAt'))['QuitGameAt__max']
        if lastViolation is None:
            return True
        else:
            delta = datetime.today() - lastViolation
            return (delta.days > 0) or (60 < delta.seconds)

from django import forms
class LobbyForm(forms.ModelForm):
    class Meta:
        model = Lobby
        fields = ['Name', 'Capacity', 'Gamers']
    def __init__(self, *args, **kwargs):
        self.user = kwargs.pop('user', None)
        super(LobbyForm, self).__init__(*args, **kwargs)

class UserForm(forms.ModelForm):
    password = forms.CharField(widget=forms.PasswordInput())

    class Meta:
        model = User
        fields = ('username', 'email', 'password')

