from django.http import HttpResponse
from django.template import loader
from django.contrib.auth.decorators import login_required
from django.shortcuts import redirect
from .models import Lobby, LobbyForm, UserForm

@login_required
def index(request):
    if request.method == 'POST':
        return redirect('create')
    else:
        lobbyList = Lobby.objects.order_by('-Created')
        template = loader.get_template('index.html')
        context = {'lobbyList': lobbyList}
        return HttpResponse(template.render(context, request))

from django.http import Http404
from django.shortcuts import render

@login_required
def detail(request, lobbyId):
    try:
        lobby = Lobby.objects.get(id=lobbyId)
 
        if request.method == 'POST':
            form = LobbyForm(request.POST, user=request.user)
            if 'Join' in form.data:
                lobby.AddGamer(request.user)
            if 'Leave' in form.data:
                lobby.RemoveGamer(request.user)
            if 'Start' in form.data:
                lobby.Start()
                        
        return render(
            request,
            'detail.html',
            {'lobby': lobby, 'currentUserIn': lobby.HasGamer(request.user), 'canStart': request.user == lobby.Creator and 0 == lobby.State})
    except Lobby.DoesNotExist:
        raise Http404('Game does not exist.')

@login_required
def create(request):
    if request.method == 'POST':
        form = LobbyForm(request.POST)
        if form.is_valid():
            lobby = Lobby()
            lobby.Name = form.cleaned_data['Name']
            lobby.Capacity = form.cleaned_data['Capacity']
            lobby.Creator = request.user
            lobby.State = 0
            lobby.save()
            return redirect('/')
        else:
            return render(request, 'create.html', {'form': form})

    else:
        form = LobbyForm()
        return render(request, 'create.html', {'form': form})

from django.template import RequestContext
from django.shortcuts import render_to_response

def register(request):
    context = RequestContext(request)
    registered = False

    if request.method == 'POST':
        user_form = UserForm(data=request.POST)
        if user_form.is_valid():
            user = user_form.save()
            user.set_password(user.password)
            user.save()
            registered = True
        else:
            print(user_form.errors)

    else:
        user_form = UserForm()

    return render_to_response(
            'registration/register.html',
            {'user_form': user_form, 'registered': registered},
            context)
       